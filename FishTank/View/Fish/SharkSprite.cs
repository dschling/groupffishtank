﻿using System;
using System.Drawing;
using FishTank.Utility.EnumeratedTypes;

namespace FishTank.View.Fish
{
    /// <summary>
    ///     Responsible for drawing the actual sprite on the screen.
    /// </summary>
    public class SharkSprite : FishSprite
    {
        #region Data members

        private const int OriginalEyeXLocationWhenFacingRight = 35;
        private const int OriginalEyeYLocation = 35;

        private Point[] rightFacingBodyCoordinates;
        private int eyeSize;
        private int eyeXLocationWhenFacingRight;
        private int eyeYLocation;
        private int eyeXLocationWhenFacingLeft;
        private int eyeYLocationWhenFacingLeft;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="FishTank.View.Fish.SharkSprite" /> class.
        ///     Precondition: fish != null
        /// </summary>
        /// <param name="fish">The fish.</param>
        public SharkSprite(Model.Fish.Fish fish) : base(fish, 130, 50)
        {
            this.initlizeFish();
        }

        #endregion

        private void initlizeFish()
        {
            this.rightFacingBodyCoordinates = new[]
            {
                new Point {X = 0, Y = 5},
                new Point {X = 30, Y = 15},
                new Point {X = 40, Y = 10},
                new Point {X = 50, Y = 7},
                new Point {X = 60, Y = 0},
                new Point {X = 70, Y = 7},
                new Point {X = 90, Y = 12},
                new Point {X = 100, Y = 15},
                new Point {X = 115, Y = 22},
                new Point {X = 130, Y = 25},
                new Point {X = 115, Y = 28},
                new Point {X = 100, Y = 35},
                new Point {X = 90, Y = 40},
                new Point {X = 70, Y = 43},
                new Point {X = 60, Y = 50},
                new Point {X = 50, Y = 43},
                new Point {X = 40, Y = 40},
                new Point {X = 30, Y = 35},
                new Point {X = 0, Y = 45},
                new Point {X = 15, Y = 25}
            };

            this.eyeSize = 5;
            this.eyeXLocationWhenFacingRight = OriginalEyeXLocationWhenFacingRight;
            this.eyeYLocation = OriginalEyeYLocation;
            this.eyeXLocationWhenFacingLeft = 95;
            this.eyeYLocationWhenFacingLeft = 35;
            this.ScaleBody();
        }

        /// <summary>
        ///     This will scale the body of the wish in accordence with it's weight
        /// </summary>
        protected override void ScaleBody()
        {
            base.ScaleBody();
            this.rightFacingBodyCoordinates = ResizeCoordinates(this.rightFacingBodyCoordinates);
            this.eyeSize = (int) Math.Round(ScaleFactor*this.eyeSize);
            this.eyeXLocationWhenFacingRight = Width - (int) Math.Round(ScaleFactor*this.eyeXLocationWhenFacingRight);
            this.eyeYLocation = Height - (int) Math.Round(ScaleFactor*this.eyeYLocation);
            this.eyeXLocationWhenFacingLeft = Width - (int) Math.Round(ScaleFactor*this.eyeXLocationWhenFacingLeft);
            this.eyeYLocationWhenFacingLeft = Height - (int) Math.Round(ScaleFactor*this.eyeYLocationWhenFacingLeft);
        }

        /// <summary>
        ///     Draws a fish
        ///     Preconditon: graphics != null
        /// </summary>
        /// <param name="graphics">The graphics object to draw the fish one</param>
        public override void Paint(Graphics graphics)
        {
            base.Paint(graphics);

            var offsetX = X;
            var offsetY = Y;

            var blackBrush = new SolidBrush(Color.Black);
            FillShape(graphics, this.getBodyCoordinates(), offsetX, offsetY, blackBrush);
            var redBrush = new SolidBrush(Color.Red);
            this.drawEye(graphics, redBrush, offsetX, offsetY);
        }

        private Point[] getBodyCoordinates()
        {
            return TheDirection == FishDirection.Right
                ? this.rightFacingBodyCoordinates
                : this.getLeftFacingBodyCoordinates();
        }

        private void drawEye(Graphics graphics, Brush brush, int offsetX, int offsetY)
        {
            if (TheDirection == FishDirection.Right)
            {
                graphics.FillEllipse(brush, this.eyeXLocationWhenFacingRight + offsetX, this.eyeYLocation + offsetY,
                    this.eyeSize, this.eyeSize);
            }
            else
            {
                graphics.FillEllipse(brush, this.eyeXLocationWhenFacingLeft + offsetX,
                    this.eyeYLocationWhenFacingLeft + offsetY, this.eyeSize, this.eyeSize);
            }
        }

        private Point[] getLeftFacingBodyCoordinates()
        {
            var leftFacingBodyCoordinates = FlipPointsXCoordinate(this.rightFacingBodyCoordinates);
            return leftFacingBodyCoordinates;
        }
    }
}