﻿using System.Drawing;
using FishTank.Properties;
using FishTank.Utility.EnumeratedTypes;

namespace FishTank.View.Fish
{
    /// <summary>
    ///     Responsible for displaying the rightFacingSprite on the screen.
    /// </summary>
    public class BlueFishSprite : FishSprite
    {
        #region Data members

        private Image rightFacingSprite;
        private Image leftFacingSprite;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="BlueFishSprite" /> class.
        /// </summary>
        /// <param name="fish">The fish.</param>
        public BlueFishSprite(Model.Fish.Fish fish)
            : base(fish, Resources.BlueFishImage.Width, Resources.BlueFishImage.Height)
        {
            this.initializeFish();
        }

        #endregion

        private void initializeFish()
        {
            this.rightFacingSprite = Resources.BlueFishImage;

            this.leftFacingSprite = Resources.BlueFishImage;
            this.leftFacingSprite.RotateFlip(RotateFlipType.RotateNoneFlipX);

            ScaleBody();
        }

        /// <summary>
        ///     Paints the specified graphics.
        /// </summary>
        /// <param name="graphics">The graphics.</param>
        public override void Paint(Graphics graphics)
        {
            base.Paint(graphics);
            var offsetX = X;
            var offsetY = Y;

            this.drawFish(graphics, this.getFishToDraw(), offsetX, offsetY);
        }

        private void setImageToDraw()
        {
            if (IsDead)
            {
                this.rightFacingSprite = Resources.DeadBlueFish;
                this.rightFacingSprite.RotateFlip(RotateFlipType.RotateNoneFlipY);

                this.leftFacingSprite = Resources.DeadBlueFish;
                this.leftFacingSprite.RotateFlip(RotateFlipType.RotateNoneFlipY);
            }
            else if (BiteCount == 2)
            {
                this.rightFacingSprite = Resources.TwoBiteBlueFish;
                this.leftFacingSprite = Resources.TwoBiteBlueFish;
            }
            else if (BiteCount == 1)
            {
                this.rightFacingSprite = Resources.OneBiteBluefish;
                this.leftFacingSprite = Resources.OneBiteBluefish;
            }
            else
            {
                this.rightFacingSprite = Resources.BlueFishImage;
                this.leftFacingSprite = Resources.BlueFishImage;
            }

            this.leftFacingSprite.RotateFlip(RotateFlipType.RotateNoneFlipX);
        }

        private Image getFishToDraw()
        {
            this.setImageToDraw();
            if (TheDirection == FishDirection.Right)
            {
                return this.rightFacingSprite;
            }

            return this.leftFacingSprite;
        }

        private void drawFish(Graphics graphics, Image image, int offsetX, int offsetY)
        {
            graphics.DrawImage(image, offsetX, offsetY,
                Width,
                Height);
        }
    }
}