﻿using System;
using System.Drawing;

namespace FishTank.View.Fish
{
    /// <summary>
    ///     Responsible for drawing the actual sprite on the screen.
    /// </summary>
    public class SpottedGoldFishSprite : GoldFishSprite
    {
        #region Data members

        private const int OriginalSpotSize = 10;

        private int spotSize;
        private int spotYLocation;
        private int spotXLocation;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="SpottedGoldFishSprite" /> class.
        /// </summary>
        /// <param name="fish">a fish.</param>
        public SpottedGoldFishSprite(Model.Fish.Fish fish) : base(fish)
        {
        }

        #endregion

        /// <summary>
        ///     Scales the body.
        /// </summary>
        protected override void ScaleBody()
        {
            base.ScaleBody();
            this.spotSize = OriginalSpotSize;
            this.spotSize = (int) Math.Round(ScaleFactor*this.spotSize);
            this.spotYLocation = (Height/2) - (this.spotSize/2);
            this.spotXLocation = (Width/2) - (this.spotSize/2);
        }

        /// <summary>
        ///     Draws a Fish object.
        ///     Preconditon: graphics != null
        /// </summary>
        /// <param name="graphics">The graphics.</param>
        public override void Paint(Graphics graphics)
        {
            base.Paint(graphics);

            var offsetX = X;
            var offsetY = Y;
            var blackBrush = new SolidBrush(Color.Black);
            graphics.FillEllipse(blackBrush, this.spotXLocation + offsetX, this.spotYLocation + offsetY,
                this.spotSize,
                this.spotSize);
        }
    }
}