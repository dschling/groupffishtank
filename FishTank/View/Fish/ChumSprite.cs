﻿using System.Drawing;

namespace FishTank.View.Fish
{
    internal class ChumSprite : FishSprite
    {
        #region Data members

        private const int Size = 5;

        #endregion

        #region Constructors

        public ChumSprite(Model.Fish.Fish fish)
            : base(fish, Size, Size)
        {
        }

        #endregion

        public override void Paint(Graphics graphics)
        {
            base.Paint(graphics);
            var offsetX = X;
            var offsetY = Y;

            var goldBrush = new SolidBrush(Color.Gold);

            graphics.FillEllipse(goldBrush, offsetX, offsetY, Width,
                Height);
        }
    }
}