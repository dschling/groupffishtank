﻿using System.Drawing;
using FishTank.Utility.EnumeratedTypes;

namespace FishTank.View.Fish
{
    /// <summary>
    ///     Responsible for drawing the actual sprite on the screen.
    /// </summary>
    public class TigerSharkSprite : SharkSprite
    {
        #region Data members

        private Point[] rightFacingStripeCoordinates;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="TigerSharkSprite" /> class.
        ///     Precondition: fish != null
        /// </summary>
        /// <param name="fish">The fish.</param>
        public TigerSharkSprite(Model.Fish.Fish fish) : base(fish)
        {
            this.initializeFish();
        }

        #endregion

        private void initializeFish()
        {
            this.rightFacingStripeCoordinates = new[]
            {
                new Point {X = 50, Y = 7},
                new Point {X = 60, Y = 0},
                new Point {X = 90, Y = 40},
                new Point {X = 70, Y = 43}
            };

            this.rightFacingStripeCoordinates = ResizeCoordinates(this.rightFacingStripeCoordinates);
        }

        /// <summary>
        ///     Draws a fish
        ///     Preconditon: graphics != null
        /// </summary>
        /// <param name="graphics">The graphics object to draw the fish one</param>
        public override void Paint(Graphics graphics)
        {
            base.Paint(graphics);

            var redBrush = new SolidBrush(Color.Red);
            FillShape(graphics, this.getStripeCoordinates(), X, Y, redBrush);
        }

        private Point[] getStripeCoordinates()
        {
            if (TheDirection == FishDirection.Right)
            {
                return this.rightFacingStripeCoordinates;
            }
            return this.getLeftFacingStripeCoordinates();
        }

        private Point[] getLeftFacingStripeCoordinates()
        {
            return FlipPointsXCoordinate(this.rightFacingStripeCoordinates);
        }
    }
}