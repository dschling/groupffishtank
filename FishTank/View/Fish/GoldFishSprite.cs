﻿using System;
using System.Drawing;
using FishTank.Utility.EnumeratedTypes;

// ReSharper disable DoNotCallOverridableMethodsInConstructor

namespace FishTank.View.Fish
{
    /// <summary>
    ///     Responsible for drawing the actual sprite on the screen.
    /// </summary>
    public class GoldFishSprite : FishSprite
    {
        #region Data members

        private Point[] rightFacingTailPoints;
        private int eyeSize;
        private int eyeXLocationWhenFacingRight;
        private int eyeLocation;
        private int eyeXLocationWhenFacingLeft;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="GoldFishSprite" /> class.
        /// </summary>
        /// <param name="fish">a fish.</param>
        public GoldFishSprite(Model.Fish.Fish fish) : base(fish, 100, 40)
        {
            this.initializeFish();
        }

        #endregion

        private void initializeFish()
        {
            this.rightFacingTailPoints = new[]
            {
                new Point {X = 0, Y = 0},
                new Point {X = 20, Y = 20},
                new Point {X = 0, Y = 40}
            };
            this.eyeSize = 5;
            this.eyeXLocationWhenFacingRight = 10;
            this.eyeXLocationWhenFacingLeft = 10;
            this.ScaleBody();
        }

        /// <summary>
        ///     This will scale the body of the wish in accordence with it's weight
        /// </summary>
        protected override void ScaleBody()
        {
            base.ScaleBody();
            this.rightFacingTailPoints = ResizeCoordinates(this.rightFacingTailPoints);
            this.eyeSize = (int) Math.Round(ScaleFactor*this.eyeSize);
            this.eyeXLocationWhenFacingRight = (Width) - (int) Math.Round(ScaleFactor*this.eyeXLocationWhenFacingRight);
            this.eyeXLocationWhenFacingLeft = (int) Math.Round(ScaleFactor*this.eyeXLocationWhenFacingLeft);
            this.eyeLocation = Height/2;
        }

        /// <summary>
        ///     Draws a Fish object.
        ///     Preconditon: graphics != null
        /// </summary>
        /// <param name="graphics">The graphics.</param>
        public override void Paint(Graphics graphics)
        {
            base.Paint(graphics);
            var offsetX = X;
            var offsetY = Y;

            var blackBrush = new SolidBrush(Color.Black);
            var bodyBrush = this.getBodyBrush();

            var tailCoordinates = this.getTailCoordinates();

            FillShape(graphics, tailCoordinates, offsetX, offsetY, bodyBrush);
            graphics.FillEllipse(bodyBrush, offsetX, offsetY, Width,
                Height);

            this.drawEye(graphics, blackBrush, offsetX, offsetY);
        }

        /// <summary>
        ///     Gets a body brush based on the number of times
        ///     the fish has been bitten.
        /// </summary>
        /// <returns>SolidBrush</returns>
        private SolidBrush getBodyBrush()
        {
            if (IsDead)
            {
                return new SolidBrush(Color.Black);
            }
            if (BiteCount == 1)
            {
                return new SolidBrush(Color.Orange);
            }
            if (BiteCount == 2)
            {
                return new SolidBrush(Color.Red);
            }
            return new SolidBrush(Color.Gold);
        }

        private Point[] getTailCoordinates()
        {
            if (TheDirection == FishDirection.Right)
            {
                return this.rightFacingTailPoints;
            }
            return this.geLeftFacingtTailCoordinates();
        }

        private Point[] geLeftFacingtTailCoordinates()
        {
            return FlipPointsXCoordinate(this.rightFacingTailPoints);
        }

        private void drawEye(Graphics graphics, Brush brush, int offsetX, int offsetY)
        {
            if (TheDirection == FishDirection.Right)
            {
                graphics.FillEllipse(brush, this.eyeXLocationWhenFacingRight + offsetX,
                    this.eyeLocation + offsetY, this.eyeSize, this.eyeSize);
            }
            else
            {
                graphics.FillEllipse(brush, this.eyeXLocationWhenFacingLeft + offsetX,
                    this.eyeLocation + offsetY, this.eyeSize, this.eyeSize);
            }
        }
    }
}