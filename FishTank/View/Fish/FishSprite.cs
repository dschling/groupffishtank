﻿using System;
using System.Drawing;
using System.Linq;
using FishTank.Utility.EnumeratedTypes;

namespace FishTank.View.Fish
{
    /// <summary>
    ///     Abstract class for other fish sprites.
    /// </summary>
    public abstract class FishSprite
    {
        #region Data members

        private const int UpperScaleLimit = 100;
        private const int LowerScaleLimit = 40;

        private readonly Model.Fish.Fish theFish;

        #endregion

        #region Properties

        /// <summary>
        ///     This is the factor that the fish should scale it's body by
        /// </summary>
        protected double ScaleFactor { get; private set; }

        /// <summary>
        ///     Gets the width.
        /// </summary>
        /// <value>
        ///     The width.
        /// </value>
        public int Width { get; protected set; }

        /// <summary>
        ///     Gets the height.
        /// </summary>
        /// <value>
        ///     The height.
        /// </value>
        public int Height { get; protected set; }

        /// <summary>
        ///     Gets the x.
        /// </summary>
        /// <value>
        ///     The x.
        /// </value>
        public int X
        {
            get { return this.theFish.X; }
        }

        /// <summary>
        ///     Gets the y.
        /// </summary>
        /// <value>
        ///     The y.
        /// </value>
        public int Y
        {
            get { return this.theFish.Y; }
        }

        /// <summary>
        ///     Gets a value indicating whether this instance is right facing.
        /// </summary>
        /// <value>
        ///     <c>true</c> if this instance is right facing; otherwise, <c>false</c>.
        /// </value>
        protected FishDirection TheDirection
        {
            get { return this.theFish.FishDirection; }
        }

        /// <summary>
        ///     Gets a value indicating whether this instance is dead.
        /// </summary>
        /// <value>
        ///     <c>true</c> if this instance is dead; otherwise, <c>false</c>.
        /// </value>
        protected bool IsDead
        {
            get { return this.theFish.IsDead; }
        }

        /// <summary>
        ///     Gets the bite count.
        /// </summary>
        /// <value>
        ///     The bite count.
        /// </value>
        protected int BiteCount
        {
            get { return this.theFish.BiteCount; }
        }

        #endregion

        #region Constructors

        /// <summary>
        ///     Prevents a default instance of the <see cref="FishSprite" /> class from being created.
        /// </summary>
        private FishSprite()
        {
            this.theFish = null;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="FishSprite" /> class.
        /// </summary>
        /// <param name="fish">a fish.</param>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        /// <exception cref="ArgumentNullException">fish</exception>
        /// <exception cref="System.ArgumentNullException">fish</exception>
        protected FishSprite(Model.Fish.Fish fish, int width, int height)
        {
            if (fish == null)
            {
                throw new ArgumentNullException("fish");
            }

            this.theFish = fish;
            this.Width = width;
            this.Height = height;
            this.setScaleFactor();
        }

        #endregion

        /// <summary>
        ///     Scales a factor based on the fish's weight
        /// </summary>
        private void setScaleFactor()
        {
            var percent = (this.theFish.Weight - this.theFish.MinWeight)/
                          (this.theFish.MaxWeight - this.theFish.MinWeight)*(UpperScaleLimit - LowerScaleLimit) +
                          LowerScaleLimit;
            percent = percent*.01;
            this.ScaleFactor = percent;
        }

        private void scaleFishDimensions()
        {
            this.scaleHeight();
            this.scaleWidth();
        }

        /// <summary>
        ///     This will scale the body of the wish in accordence with it's weight
        /// </summary>
        protected virtual void ScaleBody()
        {
            this.scaleFishDimensions();
        }

        /// <summary>
        ///     Paints the specified graphics.
        /// </summary>
        /// <param name="graphics">The graphics.</param>
        /// <exception cref="ArgumentNullException">graphics</exception>
        public virtual void Paint(Graphics graphics)
        {
            if (graphics == null)
            {
                throw new ArgumentNullException("graphics");
            }
        }

        /// <summary>
        ///     Fills the shape.
        /// </summary>
        /// <param name="graphics">The graphics.</param>
        /// <param name="coordinates">The coordinates.</param>
        /// <param name="offsetX">The offset x.</param>
        /// <param name="offsetY">The offset y.</param>
        /// <param name="brush">The brush.</param>
        public void FillShape(Graphics graphics, Point[] coordinates, int offsetX, int offsetY, SolidBrush brush)
        {
            var adjustedCoordinates =
                coordinates.Select(point => new Point(point.X + offsetX, point.Y + offsetY)).ToArray();
            graphics.FillPolygon(brush, adjustedCoordinates);
        }

        /// <summary>
        ///     Flips the points of a fish
        /// </summary>
        /// <param name="points">The points.</param>
        /// <returns>
        ///     Point[].
        /// </returns>
        protected Point[] FlipPointsXCoordinate(Point[] points)
        {
            var flippedCoordinates =
                points.Select(point => new Point(point.X*(-1) + this.Width, point.Y)).ToArray();
            return flippedCoordinates;
        }

        /// <summary>
        ///     Flips the points y coordinate.
        /// </summary>
        /// <param name="points">The points.</param>
        /// <returns></returns>
        protected Point[] FlipPointsYCoordinate(Point[] points)
        {
            var flippedCoordinates =
                points.Select(point => new Point(point.X, point.Y*(-1) + this.Height)).ToArray();
            return flippedCoordinates;
        }

        /// <summary>
        ///     Resizes the fish.
        /// </summary>
        /// <param name="points">The points.</param>
        /// <returns></returns>
        protected Point[] ResizeCoordinates(Point[] points)
        {
            var scaledCoordinates =
                points.Select(
                    point =>
                        new Point((int) Math.Round(this.ScaleFactor*point.X),
                            (int) Math.Round(this.ScaleFactor*point.Y))).ToArray();
            return scaledCoordinates;
        }

        private void scaleWidth()
        {
            this.Width = (int) Math.Round(this.ScaleFactor*this.Width);
        }

        private void scaleHeight()
        {
            this.Height = (int) Math.Round(this.ScaleFactor*this.Height);
        }
    }
}