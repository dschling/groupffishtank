using System.ComponentModel;
using System.Windows.Forms;

namespace FishTank.View.Forms
{
    partial class FishTank : Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FishTank));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.fishTankPictureBox = new System.Windows.Forms.PictureBox();
            this.animationTimer = new System.Windows.Forms.Timer(this.components);
            this.fishDataGridView = new System.Windows.Forms.DataGridView();
            this.FishTypeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FishWeightColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.addFishButton = new System.Windows.Forms.Button();
            this.numOfFishToAdd = new System.Windows.Forms.NumericUpDown();
            this.numOfFishLabel = new System.Windows.Forms.Label();
            this.emptyTankButton = new System.Windows.Forms.Button();
            this.feedFishButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.fishTankPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fishDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numOfFishToAdd)).BeginInit();
            this.SuspendLayout();
            // 
            // fishTankPictureBox
            // 
            this.fishTankPictureBox.BackColor = System.Drawing.Color.MediumBlue;
            this.fishTankPictureBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("fishTankPictureBox.BackgroundImage")));
            this.fishTankPictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.fishTankPictureBox.Location = new System.Drawing.Point(12, 12);
            this.fishTankPictureBox.Name = "fishTankPictureBox";
            this.fishTankPictureBox.Size = new System.Drawing.Size(700, 450);
            this.fishTankPictureBox.TabIndex = 0;
            this.fishTankPictureBox.TabStop = false;
            this.fishTankPictureBox.Paint += new System.Windows.Forms.PaintEventHandler(this.fishTankCanvasPictureBox_Paint);
            this.fishTankPictureBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.fishTankPictureBox_OnMouseDown);
            // 
            // animationTimer
            // 
            this.animationTimer.Interval = 200;
            this.animationTimer.Tick += new System.EventHandler(this.animationTimer_Tick);
            // 
            // fishDataGridView
            // 
            this.fishDataGridView.AllowUserToAddRows = false;
            this.fishDataGridView.AllowUserToDeleteRows = false;
            this.fishDataGridView.AllowUserToOrderColumns = true;
            this.fishDataGridView.AllowUserToResizeColumns = false;
            this.fishDataGridView.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.fishDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.fishDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.fishDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.FishTypeColumn,
            this.FishWeightColumn});
            this.fishDataGridView.Location = new System.Drawing.Point(728, 12);
            this.fishDataGridView.Name = "fishDataGridView";
            this.fishDataGridView.ReadOnly = true;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.fishDataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.fishDataGridView.RowHeadersVisible = false;
            this.fishDataGridView.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Lucida Console", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fishDataGridView.RowTemplate.ReadOnly = true;
            this.fishDataGridView.Size = new System.Drawing.Size(303, 450);
            this.fishDataGridView.TabIndex = 1;
            this.fishDataGridView.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.fishGridView_ColumnHeaderMouseClick);
            // 
            // FishTypeColumn
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            dataGridViewCellStyle2.Format = "N3";
            dataGridViewCellStyle2.NullValue = null;
            this.FishTypeColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this.FishTypeColumn.HeaderText = "Fish Type";
            this.FishTypeColumn.Name = "FishTypeColumn";
            this.FishTypeColumn.ReadOnly = true;
            this.FishTypeColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.FishTypeColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.FishTypeColumn.Width = 150;
            // 
            // FishWeightColumn
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.FishWeightColumn.DefaultCellStyle = dataGridViewCellStyle3;
            this.FishWeightColumn.HeaderText = "Fish Weight";
            this.FishWeightColumn.Name = "FishWeightColumn";
            this.FishWeightColumn.ReadOnly = true;
            this.FishWeightColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.FishWeightColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.FishWeightColumn.Width = 150;
            // 
            // addFishButton
            // 
            this.addFishButton.Location = new System.Drawing.Point(501, 484);
            this.addFishButton.Name = "addFishButton";
            this.addFishButton.Size = new System.Drawing.Size(75, 23);
            this.addFishButton.TabIndex = 2;
            this.addFishButton.Text = "Add Fish";
            this.addFishButton.UseVisualStyleBackColor = true;
            this.addFishButton.Click += new System.EventHandler(this.addFishButton_Click);
            // 
            // numOfFishToAdd
            // 
            this.numOfFishToAdd.Location = new System.Drawing.Point(429, 484);
            this.numOfFishToAdd.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.numOfFishToAdd.Name = "numOfFishToAdd";
            this.numOfFishToAdd.Size = new System.Drawing.Size(46, 20);
            this.numOfFishToAdd.TabIndex = 3;
            // 
            // numOfFishLabel
            // 
            this.numOfFishLabel.AutoSize = true;
            this.numOfFishLabel.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numOfFishLabel.Location = new System.Drawing.Point(151, 490);
            this.numOfFishLabel.Name = "numOfFishLabel";
            this.numOfFishLabel.Size = new System.Drawing.Size(272, 14);
            this.numOfFishLabel.TabIndex = 4;
            this.numOfFishLabel.Text = "How Many Fish Do you Want To Add To The Tank?";
            // 
            // emptyTankButton
            // 
            this.emptyTankButton.Location = new System.Drawing.Point(582, 484);
            this.emptyTankButton.Name = "emptyTankButton";
            this.emptyTankButton.Size = new System.Drawing.Size(75, 23);
            this.emptyTankButton.TabIndex = 5;
            this.emptyTankButton.Text = "Empty Tank";
            this.emptyTankButton.UseVisualStyleBackColor = true;
            this.emptyTankButton.Click += new System.EventHandler(this.emptyTankButton_Click);
            // 
            // feedFishButton
            // 
            this.feedFishButton.Enabled = false;
            this.feedFishButton.Location = new System.Drawing.Point(663, 484);
            this.feedFishButton.Name = "feedFishButton";
            this.feedFishButton.Size = new System.Drawing.Size(75, 23);
            this.feedFishButton.TabIndex = 6;
            this.feedFishButton.Text = "Feed Fish";
            this.feedFishButton.UseVisualStyleBackColor = true;
            this.feedFishButton.Click += new System.EventHandler(this.feedFishButton_Click);
            // 
            // FishTank
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1054, 516);
            this.Controls.Add(this.feedFishButton);
            this.Controls.Add(this.emptyTankButton);
            this.Controls.Add(this.numOfFishLabel);
            this.Controls.Add(this.numOfFishToAdd);
            this.Controls.Add(this.addFishButton);
            this.Controls.Add(this.fishDataGridView);
            this.Controls.Add(this.fishTankPictureBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "FishTank";
            this.Text = "FishTank A5 by Dalton Schling and David Anderson";
            ((System.ComponentModel.ISupportInitialize)(this.fishTankPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fishDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numOfFishToAdd)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private PictureBox fishTankPictureBox;
        private Timer animationTimer;
        private DataGridView fishDataGridView;
        private Button addFishButton;
        private NumericUpDown numOfFishToAdd;
        private Label numOfFishLabel;
        private Button emptyTankButton;
        private DataGridViewTextBoxColumn FishTypeColumn;
        private DataGridViewTextBoxColumn FishWeightColumn;
        private Button feedFishButton;
    }
}

