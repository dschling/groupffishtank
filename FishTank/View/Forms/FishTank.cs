using System;
using System.Linq;
using System.Windows.Forms;
using FishTank.Model;

namespace FishTank.View.Forms
{
    /// <summary>
    ///     Manages the form that displays the fish tank.
    /// </summary>
    public partial class FishTank
    {
        #region Data members

        private const int FishTypeColumnIndex = 0;
        private const int FishWeightColumnIndex = 1;
        private bool ascending;
        private readonly FishTankManager fishTankManager;
        private double totalFishWeight;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="FishTank" /> class.
        ///     Precondition: None
        /// </summary>
        public FishTank()
        {
            this.InitializeComponent();
            this.ascending = false;
            this.totalFishWeight = 0;
            this.fishTankManager = new FishTankManager(this.fishTankPictureBox.Height, this.fishTankPictureBox.Width);
            this.emptyTankButton.Enabled = false;
            this.animationTimer.Start();
        }

        #endregion

        #region Event generated methods

        private void animationTimer_Tick(object sender, EventArgs e)
        {
            Refresh();
        }

        private void fishTankCanvasPictureBox_Paint(object sender, PaintEventArgs e)
        {
            var graphics = e.Graphics;
            this.fishTankManager.Update(graphics);
            if (this.fishTankManager.FishData.Count == 0)
            {
                return;
            }
            if (!(Math.Abs(this.fishTankManager.FishData.Sum(fish => fish.Weight) - this.totalFishWeight) > .0009))
            {
                return;
            }
            this.bindGrid();
            this.totalFishWeight = this.fishTankManager.FishData.Sum(fish => fish.Weight);
        }

        private void bindGrid()
        {
            this.fishDataGridView.Rows.Clear();

            this.fishTankManager.SortFishByTypeThenWeightDescending();
            foreach (var aFish in this.fishTankManager.FishData)
            {
                this.fishDataGridView.Rows.Add(aFish.Type.ToString(), aFish.Weight.ToString("F3") + " lb");
            }
        }

        private void addFishButton_Click(object sender, EventArgs e)
        {
            this.fishTankManager.PlaceFishInTank((int) this.numOfFishToAdd.Value);
            this.emptyTankButton.Enabled = true;
            this.feedFishButton.Enabled = true;
            this.addFishButton.Enabled = false;
            this.totalFishWeight = this.fishTankManager.FishData.Sum(fish => fish.Weight);
            this.bindGrid();
        }

        private void emptyTankButton_Click(object sender, EventArgs e)
        {
            this.fishTankManager.EmptyTank();
            this.fishDataGridView.Rows.Clear();
            this.emptyTankButton.Enabled = false;
            this.feedFishButton.Enabled = false;
            this.addFishButton.Enabled = true;
        }

        private void fishGridView_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.ColumnIndex == FishTypeColumnIndex && !this.ascending)
            {
                this.fishTankManager.SortFishByTypeThenWeightAscending();
                this.ascending = true;
            }
            else if (e.ColumnIndex == FishTypeColumnIndex && this.ascending)
            {
                this.fishTankManager.SortFishByTypeThenWeightDescending();
                this.ascending = false;
            }
            else if (e.ColumnIndex == FishWeightColumnIndex && !this.ascending)
            {
                this.fishTankManager.SortFishByWeightAscending();
                this.ascending = true;
            }
            else if (e.ColumnIndex == FishWeightColumnIndex && this.ascending)
            {
                this.fishTankManager.SortFishByWeightDescending();
                this.ascending = false;
            }

            this.bindGrid();
        }

        #endregion

        private void fishTankPictureBox_OnMouseDown(object sender, MouseEventArgs cursor)
        {
            try
            {
                foreach (var fish in this.fishTankManager.FishData)
                {
                    if (fish.FishArea.Contains(cursor.Location))
                    {
                        this.fishTankManager.HandleMouseClick(fish);
                    }
                }
            }
            catch (InvalidOperationException)
            {
                this.fishTankManager.SetBindingList();
                this.bindGrid();
            }
        }

        private void feedFishButton_Click(object sender, EventArgs e)
        {
            this.fishTankManager.FeedFish();
        }
    }
}