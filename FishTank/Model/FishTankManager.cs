﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using FishTank.Model.Fish;
using FishTank.Utility;
using FishTank.Utility.EnumeratedTypes;
using FishTank.Utility.MovementBehaviors;

namespace FishTank.Model
{
    /// <summary>
    ///     Manages the collection of fish in the tank.
    /// </summary>
    public class FishTankManager
    {
        #region Data members

        private BindingList<Fish.Fish> fish;
        private readonly List<Chum> chum;
        private readonly int height;
        private readonly int width;

        #endregion

        #region Properties

        /// <summary>
        ///     Gets the fish data.
        /// </summary>
        /// <value>
        ///     The fish data.
        /// </value>
        public BindingList<Fish.Fish> FishData { get; private set; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Prevents a default instance of the <see cref="FishTankManager" /> class from being created.
        /// </summary>
        private FishTankManager()
        {
            this.fish = new BindingList<Fish.Fish>();
            this.FishData = new BindingList<Fish.Fish>();
            this.chum = new List<Chum>();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="FishTankManager" /> class.
        ///     Precondition: None
        /// </summary>
        /// <param name="height">The height.</param>
        /// <param name="width">The width.</param>
        public FishTankManager(int height, int width) : this()
        {
            this.height = height;
            this.width = width;
        }

        #endregion

        /// <summary>
        ///     Places 3 random fish in tank.
        /// </summary>
        public void PlaceFishInTank()
        {
            this.PlaceFishInTank(3);
        }

        /// <summary>
        ///     Places the fish in the tank.
        ///     Precondition: None
        /// </summary>
        public void PlaceFishInTank(int numberOfFish)
        {
            this.fish = new BindingList<Fish.Fish>();
            for (var i = 0; i < numberOfFish; i++)
            {
                this.addFish();
            }
            this.SetBindingList();
        }

        /// <summary>
        ///     Places the chum in tank.
        /// </summary>
        public void PlaceChumInTank()
        {
            for (var counter = 0; counter < RandomUtils.NextInt(10, 100); counter++)
            {
                this.addChum();
            }
        }

        private void addChum()
        {
            var x = RandomUtils.NextInt(this.width);
            var y = 0;
            var chumToAdd = new Chum();
            if (x >= this.width - (chumToAdd.Width))
            {
                chumToAdd.X = this.width - (chumToAdd.Width);
            }
            else
            {
                chumToAdd.X = x;
            }

            if (y >= this.height - (chumToAdd.Height))
            {
                chumToAdd.Y = this.height - (chumToAdd.Height);
            }
            else
            {
                chumToAdd.Y = y;
            }

            this.chum.Add(chumToAdd);
        }

        /// <summary>
        ///     Sets the binding list.
        /// </summary>
        public void SetBindingList()
        {
            this.FishData = new BindingList<Fish.Fish>(this.fish);
        }

        private void addFish()
        {
            var fishToAdd = FishFactory.GetRandomFish();
            do
            {
                var x = RandomUtils.NextInt(this.width);
                var y = RandomUtils.NextInt(this.height);

                if (x >= this.width - (fishToAdd.Width))
                {
                    fishToAdd.X = this.width - (fishToAdd.Width);
                }
                else
                {
                    fishToAdd.X = x;
                }

                if (y >= this.height - (fishToAdd.Height))
                {
                    fishToAdd.Y = this.height - (fishToAdd.Height);
                }
                else
                {
                    fishToAdd.Y = y;
                }
            } while (this.collidesWithAnotherFish(fishToAdd));
            fishToAdd.IsBiting = false;
            fishToAdd.IsBeingBitten = false;
            this.fish.Add(fishToAdd);
        }

        private bool collidesWithAnotherFish(Fish.Fish aFish)
        {
            foreach (var fish in this.fish)
            {
                var fishAreaRectangle = new Rectangle(fish.X, fish.Y, fish.Width, fish.Height);
                var fishToAddAreaRectangle = new Rectangle(aFish.X, aFish.Y, aFish.Width, aFish.Height);

                if (fishToAddAreaRectangle.IntersectsWith(fishAreaRectangle))
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        ///     Return true if any of the fish in the tank are colliding
        /// </summary>
        /// <returns>true if any of the fish in the tank are colliding</returns>
        public bool AreFishColliding()
        {
            for (var counter = 0; counter < this.fish.Count; counter++)
            {
                var fishThatIsBeingCompared = this.fish[counter];
                this.fish.RemoveAt(counter);
                var isColliding = this.collidesWithAnotherFish(fishThatIsBeingCompared);
                counter++;
                this.fish.Add(fishThatIsBeingCompared);
                if (isColliding)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        ///     Moves the fish around and the calls the Fish::Paint method to draw the fish.
        ///     Precondition: graphics != null
        /// </summary>
        /// <param name="graphics">The graphics object to draw on</param>
        public void Update(Graphics graphics)
        {
            if (graphics == null)
            {
                throw new ArgumentNullException("graphics");
            }

            if (this.fish == null)
            {
                return;
            }
            foreach (var aFish in this.fish)
            {
                aFish.Paint(graphics);
                aFish.Move(this.width, this.height);
                this.handleCollidingFish(aFish);
                this.handleFishEating(aFish);
            }
            foreach (var aChum in this.chum)
            {
                aChum.Paint(graphics);
                aChum.Move(this.width, this.height);
            }
        }

        private void handleFishEating(Fish.Fish aFish)
        {
            if (aFish.IsAShark())
            {
                return;
            }

            foreach (var chum in this.chum.ToArray())
            {
                var feedingArea = new Rectangle(aFish.FishArea.Location.X - 5, aFish.FishArea.Location.Y,
                    aFish.FishArea.Width, aFish.FishArea.Height);

                if (aFish.FishArea.IntersectsWith(chum.FishArea))
                {
                    this.chum.Remove(chum);
                    aFish.AddBiteWeight();
                }
                if (feedingArea.IntersectsWith(chum.FishArea) &&
                    aFish.MovementBehavior.GetType() == typeof (RestingBehavior))
                {
                    aFish.Move(MovementDirection.Forward);
                }
            }
        }

        private void handleCollidingFish(Fish.Fish aFish)
        {
            if (!aFish.IsAShark())
            {
                return;
            }

            foreach (var otherFish in this.fish)
            {
                if (otherFish.IsAShark())
                {
                    this.handleCollidingSharks(aFish, otherFish);
                }
                else
                {
                    this.handleSharkBites(aFish, otherFish);
                }
            }
        }

        private void handleSharkBites(Fish.Fish aShark, Fish.Fish otherFish)
        {
            var theShark = (Shark) aShark;
            if (theShark.FishArea.IntersectsWith(otherFish.FishArea))
            {
                otherFish.HandleBite();
                theShark.HandleBite();
                if (otherFish.IsInTopHalfOfTank(this.height) &&
                    otherFish.MovementBehavior.GetType() == typeof (RestingBehavior))
                {
                    otherFish.Move(MovementDirection.Down);
                }
                else if (!otherFish.IsInTopHalfOfTank(this.height) &&
                         otherFish.MovementBehavior.GetType() == typeof (RestingBehavior))
                {
                    otherFish.Move(MovementDirection.Up);
                }
            }
            else if (!theShark.FishArea.IntersectsWith(otherFish.FishArea) && otherFish.IsBeingBitten &&
                     theShark.IsBiting)
            {
                otherFish.IsBeingBitten = false;
                theShark.IsBiting = false;
            }
            this.SetBindingList();
        }

        private void handleCollidingSharks(Fish.Fish aShark, Fish.Fish otherShark)
        {
            if (aShark.FishDirection != otherShark.FishDirection)
            {
                if (aShark.FishArea.IntersectsWith(otherShark.FishArea))
                {
                    aShark.ChangeDirection();
                    otherShark.ChangeDirection();
                }
            }
        }

        /// <summary>
        ///     Empties the tank.
        /// </summary>
        public void EmptyTank()
        {
            this.fish.Clear();
            this.chum.Clear();
        }

        /// <summary>
        ///     This will sort the fish according to their respective type and then by their weight.
        /// </summary>
        public void SortFishByTypeThenWeightAscending()
        {
            var sortedFish = this.FishData.OrderBy(afish => afish.Type).ThenBy(afish => afish.Weight);

            this.FishData = new BindingList<Fish.Fish>(sortedFish.ToList());
        }

        /// <summary>
        ///     This will sort the fish according to their respective type and then by their weight.
        /// </summary>
        public void SortFishByTypeThenWeightDescending()
        {
            var sortedFish = this.FishData.OrderByDescending(afish => afish.Type).ThenBy(afish => afish.Weight);

            this.FishData = new BindingList<Fish.Fish>(sortedFish.ToList());
        }

        /// <summary>
        ///     This will sort the fish according to their respective weight.
        /// </summary>
        public void SortFishByWeightAscending()
        {
            var sortedFish = this.FishData.OrderBy(afish => afish.Weight);

            this.FishData = new BindingList<Fish.Fish>(sortedFish.ToList());
        }

        /// <summary>
        ///     This will sort the fish according to their respective weight.
        /// </summary>
        public void SortFishByWeightDescending()
        {
            var sortedFish = this.FishData.OrderByDescending(afish => afish.Weight);

            this.FishData = new BindingList<Fish.Fish>(sortedFish.ToList());
        }

        /// <summary>
        ///     Feeds the fish.
        /// </summary>
        public void FeedFish()
        {
            this.PlaceChumInTank();
        }

        /// <summary>
        ///     Handles the mouse click.
        /// </summary>
        /// <param name="clickedFish">The clicked fish.</param>
        public void HandleMouseClick(Fish.Fish clickedFish)
        {
            if (clickedFish.IsFeeding || clickedFish.IsSleeping)
            {
                clickedFish.IsClicked = true;
            }
            if (clickedFish.IsDead)
            {
                this.fish.Remove(clickedFish);
                this.SetBindingList();
            }
        }
    }
}