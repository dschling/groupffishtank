﻿using System.Drawing;
using FishTank.Utility;
using FishTank.Utility.EnumeratedTypes;
using FishTank.Utility.MovementBehaviors;
using FishTank.View.Fish;

namespace FishTank.Model.Fish
{
    /// <summary>
    ///     Holds information about a Shark
    /// </summary>
    public class Shark : Fish
    {
        #region Data members

        private const int MaxValueToMove = 450;
        private int randomHeightForSharkToRise;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="Shark" /> class.
        /// </summary>
        public Shark()
        {
            this.initializeFish();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="Shark" /> class.
        /// </summary>
        /// <param name="direction">The direction.</param>
        public Shark(FishDirection direction) : base(direction)
        {
            this.initializeFish();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="Shark" /> class.
        /// </summary>
        /// <param name="location">Location to create fish</param>
        /// <param name="direction">The direction.</param>
        public Shark(Point location, FishDirection direction)
            : base(location, direction)
        {
            this.initializeFish();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="Shark" /> class.
        /// </summary>
        /// <param name="x">The x coordinate</param>
        /// <param name="y">The y coordinate</param>
        /// <param name="direction">The direction.</param>
        public Shark(int x, int y, FishDirection direction)
            : base(x, y, direction)
        {
            this.initializeFish();
        }

        #endregion

        private void initializeFish()
        {
            MaxWeight = 45.0;
            MinWeight = 12.0;
            SetRandomWeight();
            Type = FishTypes.Shark;
            this.SetDefaultMovement();
            FishSprite = new SharkSprite(this);
            FishArea = new Rectangle(X, Y, Width, Height);
            IsBiting = false;
            this.randomHeightForSharkToRise = RandomUtils.NextInt(MaxValueToMove);
        }

        public override void Move(int tankWidth, int tankHeight)
        {
            this.handleSwimBehaviorForWallHits(tankHeight);
            base.Move(tankWidth, tankHeight);
        }

        /// <summary>
        ///     Updates the movement behavior.
        /// </summary>
        private void handleSwimBehaviorForWallHits(int tankHeight)
        {
            if (IsSleeping && !IsAtBottom(tankHeight))
            {
                MovementBehavior = new SwimToBottomBehavior();
            }

            if (IsAtBottom(tankHeight) && IsSleeping && !IsClicked)
            {
                MovementBehavior = new RestingBehavior();
            }

            if (IsAtBottom(tankHeight) && IsSleeping && IsClicked)
            {
                MovementBehavior = new SwimToTopBehavior();
                IsClicked = false;
                IsSleeping = false;
                ClearWallHits();
            }

            if (!IsSleeping && !IsClicked && Y <= this.randomHeightForSharkToRise)
            {
                MovementBehavior = new AggresiveBehavior();
            }
        }

        public override void SetDefaultMovement()
        {
            MovementBehavior = new AggresiveBehavior();
        }

        public override void AddBiteWeight()
        {
            Weight = Weight + (Weight*.02);
        }

        public override void HandleBite()
        {
            if (IsBiting)
            {
                return;
            }
            this.AddBiteWeight();
            IsBiting = true;
        }
    }
}