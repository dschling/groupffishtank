﻿using System.Drawing;
using FishTank.Utility.EnumeratedTypes;
using FishTank.Utility.MovementBehaviors;
using FishTank.View.Fish;

namespace FishTank.Model.Fish
{
    /// <summary>
    ///     Holds information about a GoldFish
    /// </summary>
    public class GoldFish : Fish
    {
        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="GoldFish" /> class.
        /// </summary>
        public GoldFish()
        {
            this.initializeFish();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="GoldFish" /> class.
        /// </summary>
        /// <param name="direction">The direction.</param>
        public GoldFish(FishDirection direction) : base(direction)
        {
            this.initializeFish();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="GoldFish" /> class.
        /// </summary>
        /// <param name="location">Location to create fish</param>
        /// <param name="direction">The direction.</param>
        public GoldFish(Point location, FishDirection direction) : base(location, direction)
        {
            this.initializeFish();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="GoldFish" /> class.
        /// </summary>
        /// <param name="x">The x coordinate</param>
        /// <param name="y">The y coordinate</param>
        /// <param name="direction">The direction.</param>
        public GoldFish(int x, int y, FishDirection direction) : base(x, y, direction)
        {
            this.initializeFish();
        }

        #endregion

        private void initializeFish()
        {
            MaxWeight = 2.5;
            MinWeight = 0.25;
            SetRandomWeight();
            this.SetDefaultMovement();
            FishSprite = new GoldFishSprite(this);
            Type = FishTypes.GoldFish;
            FishArea = new Rectangle(X, Y, Width, Height);
            IsSleeping = false;
            IsClicked = false;
        }

        /// <summary>
        ///     Sets the default movement.
        /// </summary>
        public override void SetDefaultMovement()
        {
            MovementBehavior = new RestingBehavior();
        }
    }
}