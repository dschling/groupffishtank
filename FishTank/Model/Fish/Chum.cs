﻿using System.Drawing;
using FishTank.Utility.EnumeratedTypes;
using FishTank.Utility.MovementBehaviors;
using FishTank.View.Fish;

namespace FishTank.Model.Fish
{
    internal class Chum : Fish
    {
        #region Constructors

        public Chum()
        {
            this.initializeChum();
        }

        #endregion

        private void initializeChum()
        {
            this.SetDefaultMovement();
            FishSprite = new ChumSprite(this);
            Type = FishTypes.Chum;
            FishArea = new Rectangle(X, Y, Width, Height);
        }

        public override void SetDefaultMovement()
        {
            MovementBehavior = new SwimToBottomBehavior();
        }

        public override void HandleBite()
        {
            IsDead = true;
        }

        protected override void HandleDeadFishMovement()
        {
            this.SetDefaultMovement();
        }

        public override void Move(int tankWidth, int tankHeight)
        {
            base.Move(tankWidth, tankHeight);
            if (IsAtBottom(tankHeight))
            {
                MovementBehavior = new NoMovementBehavior();
            }
        }
    }
}