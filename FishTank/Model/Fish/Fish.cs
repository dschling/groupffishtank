using System;
using System.Drawing;
using FishTank.Utility;
using FishTank.Utility.EnumeratedTypes;
using FishTank.Utility.MovementBehaviors;
using FishTank.View.Fish;

namespace FishTank.Model.Fish
{
    /// <summary>
    ///     Holds information about a fish
    /// </summary>
    public abstract class Fish
    {
        #region Data members

        private const int ForwardMovement = 5;

        private Point location;
        private FishSprite sprite;
        private int wallHits;

        #endregion

        #region Properties

        /// <summary>
        ///     Gets or sets the FishType.
        /// </summary>
        /// <value>
        ///     The type.
        /// </value>
        public FishTypes Type { get; protected set; }

        /// <summary>
        ///     Gets or sets the weight of the fish.
        /// </summary>
        /// <value>
        ///     The weight.
        /// </value>
        public double Weight { get; set; }

        /// <summary>
        ///     Gets or sets the maximum weight.
        /// </summary>
        /// <value>
        ///     The maximum weight.
        /// </value>
        public double MaxWeight { get; protected set; }

        /// <summary>
        ///     Gets or sets the minimum weight.
        /// </summary>
        /// <value>
        ///     The minimum weight.
        /// </value>
        public double MinWeight { get; protected set; }

        /// <summary>
        ///     Gets or sets the x location of the fish.
        /// </summary>
        /// <value>
        ///     The x.
        /// </value>
        public int X
        {
            get { return this.location.X; }
            set { this.location.X = value; }
        }

        /// <summary>
        ///     Gets or sets the y location of the fish.
        /// </summary>
        /// <value>
        ///     The y.
        /// </value>
        public int Y
        {
            get { return this.location.Y; }
            set { this.location.Y = value; }
        }

        /// <summary>
        ///     Gets the width.
        /// </summary>
        /// <value>
        ///     The width.
        /// </value>
        public int Width
        {
            get { return this.sprite.Width; }
        }

        /// <summary>
        ///     Gets the height.
        /// </summary>
        /// <value>
        ///     The height.
        /// </value>
        public int Height
        {
            get { return this.sprite.Height; }
        }

        /// <summary>
        ///     Sets the sprite.
        /// </summary>
        /// <value>
        ///     The sprite.
        /// </value>
        protected FishSprite FishSprite
        {
            set { this.sprite = value; }
        }

        /// <summary>
        ///     Gets the fish direction.
        /// </summary>
        /// <value>
        ///     The fish direction.
        /// </value>
        public FishDirection FishDirection { get; private set; }

        /// <summary>
        ///     Gets or sets the movement behavior.
        /// </summary>
        /// <value>
        ///     The movement behavior.
        /// </value>
        public MovementBehavior MovementBehavior { get; set; }

        /// <summary>
        ///     Gets or sets the fish area.
        /// </summary>
        /// <value>
        ///     The fish area.
        /// </value>
        public Rectangle FishArea { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether this instance is resting.
        /// </summary>
        /// <value>
        ///     <c>true</c> if this instance is resting; otherwise, <c>false</c>.
        /// </value>
        public bool IsSleeping { get; protected set; }

        /// <summary>
        ///     Gets or sets a value indicating whether this instance is clicked.
        /// </summary>
        /// <value>
        ///     <c>true</c> if this instance is clicked; otherwise, <c>false</c>.
        /// </value>
        public bool IsClicked { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether this instance is feeding.
        /// </summary>
        /// <value>
        ///     <c>true</c> if this instance is feeding; otherwise, <c>false</c>.
        /// </value>
        public bool IsFeeding { get; set; }

        /// <summary>
        ///     Gets the bite count.
        /// </summary>
        /// <value>
        ///     The bite count.
        /// </value>
        public int BiteCount { get; private set; }

        /// <summary>
        ///     Gets a value indicating whether this instance is dead.
        /// </summary>
        /// <value>
        ///     <c>true</c> if this instance is dead; otherwise, <c>false</c>.
        /// </value>
        public bool IsDead { get; protected set; }

        /// <summary>
        ///     Gets or sets a value indicating whether this instance is being bitten.
        /// </summary>
        /// <value>
        ///     <c>true</c> if this instance is being bitten; otherwise, <c>false</c>.
        /// </value>
        public bool IsBeingBitten { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether this instance is biting.
        /// </summary>
        /// <value>
        ///     <c>true</c> if this instance is biting; otherwise, <c>false</c>.
        /// </value>
        public bool IsBiting { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Prevents a default instance of the <see cref="Fish" /> class from being created.
        /// </summary>
        protected Fish()
        {
            this.location = new Point(0, 0);
            this.FishDirection = RandomUtils.GetRandomDirection();
            this.wallHits = 0;
            this.IsSleeping = false;
            this.IsClicked = false;
            this.IsDead = false;
            this.BiteCount = 0;
            this.IsBeingBitten = false;
            this.IsBiting = false;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="Fish" /> class in the specified direction.
        /// </summary>
        /// <param name="direction">The direction.</param>
        protected Fish(FishDirection direction) : this()
        {
            this.FishDirection = direction;
        }

        /// <summary>
        ///     Constructs a fish at specified location
        ///     Precondition: location != null
        ///     Postcondition: X == location.X; Y == location.Y
        /// </summary>
        /// <param name="location">Location to create fish</param>
        /// <param name="direction">The direction.</param>
        /// <exception cref="ArgumentNullException">location</exception>
        /// <exception cref="System.ArgumentNullException">location</exception>
        protected Fish(Point location, FishDirection direction) : this(direction)
        {
            if (location == null)
            {
                throw new ArgumentNullException("location");
            }

            this.location = location;
        }

        /// <summary>
        ///     Constructs a fish at specified x,y location and direction
        ///     Precondition: None
        ///     Postcondition: X == x; Y == y
        /// </summary>
        /// <param name="x">The x coordinate</param>
        /// <param name="y">The y coordinate</param>
        /// <param name="direction">The direction.</param>
        protected Fish(int x, int y, FishDirection direction) : this(direction)
        {
            this.location = new Point(x, y);
        }

        #endregion

        /// <summary>
        ///     Moves the Fish.
        /// </summary>
        /// <param name="tankWidth">Width of the tank.</param>
        /// <param name="tankHeight">Height of the tank.</param>
        public virtual void Move(int tankWidth, int tankHeight)
        {
            this.MovementBehavior.Move(this, tankHeight, tankWidth);
            this.updateArea();
            this.HandleSwimBehaviorForFeeding(tankHeight);
            this.HandleDeadFishMovement();
        }

        /// <summary>
        ///     Handles the dead fish movement.
        /// </summary>
        protected virtual void HandleDeadFishMovement()
        {
            if (this.IsDead)
            {
                this.MovementBehavior = new SwimToTopBehavior();
            }
        }

        /// <summary>
        ///     Handles the swim behavior for feeding.
        /// </summary>
        /// <param name="tankHeight">Height of the tank.</param>
        protected void HandleSwimBehaviorForFeeding(int tankHeight)
        {
            if (this.IsFeeding && !this.IsClicked)
            {
                this.MovementBehavior = new SwimToTopBehavior();
            }
            if (this.IsAtTop(tankHeight) && this.IsFeeding && this.IsClicked && !this.IsDead)
            {
                this.MovementBehavior = new SwimToBottomBehavior();
            }
            if (this.IsFeeding && this.IsClicked && this.Y > RandomUtils.NextInt(tankHeight - this.Height) &&
                !this.IsDead)
            {
                this.SetDefaultMovement();
                this.IsClicked = false;
                this.IsFeeding = false;
            }
        }

        private void updateArea()
        {
            this.FishArea = new Rectangle(this.X, this.Y, this.Width, this.Height);
        }

        /// <summary>
        ///     Changes the direction of the fish.
        /// </summary>
        public void ChangeDirection()
        {
            if (this.FishDirection == FishDirection.Right)
            {
                this.FishDirection = FishDirection.Left;
            }
            else
            {
                this.FishDirection = FishDirection.Right;
            }
        }

        /// <summary>
        ///     Sets the default movement.
        /// </summary>
        public abstract void SetDefaultMovement();

        /// <summary>
        ///     Randomly generates a weight for the fish.
        /// </summary>
        protected void SetRandomWeight()
        {
            var newWeight = RandomUtils.NextDouble(this.MinWeight, this.MaxWeight);

            this.Weight = newWeight;
        }

        /// <summary>
        ///     Determines whether the fish is at the bottom of the tank.
        /// </summary>
        /// <param name="tankHeight">Height of the tank.</param>
        /// <returns></returns>
        public bool IsAtBottom(int tankHeight)
        {
            return this.Y > tankHeight - this.Height - 10;
        }

        /// <summary>
        ///     Adds a wall hit.
        /// </summary>
        public void AddWallHit()
        {
            this.wallHits += 1;
            if (this.wallHits >= 3)
            {
                this.IsSleeping = true;
            }
        }

        /// <summary>
        ///     Clears the wall hits.
        /// </summary>
        public void ClearWallHits()
        {
            this.wallHits = 0;
        }

        /// <summary>
        ///     Determines whether the fish is at the bottom of the tank.
        /// </summary>
        /// <param name="tankHeight">Height of the tank.</param>
        /// <returns></returns>
        public bool IsAtTop(int tankHeight)
        {
            return this.Y <= this.Height + 10;
        }

        /// <summary>
        ///     Draws a fish
        ///     Precondition: g != NULL
        /// </summary>
        /// <param name="g">The graphics object to draw the fish on</param>
        public void Paint(Graphics g)
        {
            if (g == null)
            {
                throw new ArgumentNullException("g");
            }

            this.sprite.Paint(g);
        }

        /// <summary>
        ///     Determines whether this [is a shark].
        /// </summary>
        /// <returns></returns>
        public bool IsAShark()
        {
            return this.Type == FishTypes.Shark || this.Type == FishTypes.TigerShark;
        }

        /// <summary>
        ///     Adds a bite to the biteCount.
        /// </summary>
        public void AddBite()
        {
            this.BiteCount ++;

            if (this.BiteCount >= 3)
            {
                this.IsDead = true;
            }
        }

        /// <summary>
        ///     Handles the bite.
        /// </summary>
        public virtual void HandleBite()
        {
            if (!this.IsBeingBitten)
            {
                this.AddBite();
                this.IsBeingBitten = true;
            }
            if (this.BiteCount == 2)
            {
                this.MovementBehavior = new AggresiveBehavior();
            }
        }

        /// <summary>
        ///     Determines whether [is in top half] of [the specified height].
        /// </summary>
        /// <param name="tankHeight">The tankHeight.</param>
        /// <returns></returns>
        public bool IsInTopHalfOfTank(int tankHeight)
        {
            return (this.Y < tankHeight/2);
        }

        /// <summary>
        ///     Moves the specified direction.
        /// </summary>
        /// <param name="direction">The direction.</param>
        public void Move(MovementDirection direction)
        {
            if (direction == MovementDirection.Up)
            {
                this.Y -= RandomUtils.NextInt(this.Height);
            }
            if (direction == MovementDirection.Down)
            {
                this.Y += RandomUtils.NextInt(this.Height);
            }
            if (direction == MovementDirection.Forward)
            {
                if (this.FishDirection == FishDirection.Left)
                {
                    this.X -= ForwardMovement;
                }
                else
                {
                    this.X += ForwardMovement;
                }
            }
        }

        /// <summary>
        ///     Adds the bite weight.
        /// </summary>
        public virtual void AddBiteWeight()
        {
            this.Weight = this.Weight + (this.Weight*.03);
        }
    }
}