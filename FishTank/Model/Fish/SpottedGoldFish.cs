﻿using System.Drawing;
using FishTank.Utility.EnumeratedTypes;
using FishTank.View.Fish;

namespace FishTank.Model.Fish
{
    /// <summary>
    ///     Holds information about a SpottedGoldFish
    /// </summary>
    public class SpottedGoldFish : GoldFish
    {
        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="SpottedGoldFish" /> class.
        /// </summary>
        /// <param name="direction">The direction.</param>
        public SpottedGoldFish(FishDirection direction) : base(direction)
        {
            this.initalizeFish();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="SpottedGoldFish" /> class.
        /// </summary>
        /// <param name="location">Location to create fish</param>
        /// <param name="direction">The direction.</param>
        public SpottedGoldFish(Point location, FishDirection direction) : base(location, direction)
        {
            this.initalizeFish();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="SpottedGoldFish" /> class.
        /// </summary>
        /// <param name="x">The x coordinate</param>
        /// <param name="y">The y coordinate</param>
        /// <param name="direction">The direction.</param>
        public SpottedGoldFish(int x, int y, FishDirection direction) : base(x, y, direction)
        {
            this.initalizeFish();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="SpottedGoldFish" /> class.
        /// </summary>
        public SpottedGoldFish()
        {
            this.initalizeFish();
        }

        #endregion

        private void initalizeFish()
        {
            FishSprite = new SpottedGoldFishSprite(this);
            Type = FishTypes.SpottedGoldFish;
        }
    }
}