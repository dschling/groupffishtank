﻿using System.Drawing;
using FishTank.Utility.EnumeratedTypes;
using FishTank.View.Fish;

namespace FishTank.Model.Fish
{
    /// <summary>
    ///     Holds information about a TigerShark
    /// </summary>
    public class TigerShark : Shark
    {
        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="TigerShark" /> class.
        /// </summary>
        /// <param name="direction">The direction.</param>
        public TigerShark(FishDirection direction) : base(direction)
        {
            this.initializeFish();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="TigerShark" /> class.
        /// </summary>
        /// <param name="location">Location to create fish</param>
        /// <param name="direction">The direction.</param>
        public TigerShark(Point location, FishDirection direction)
            : base(location, direction)
        {
            this.initializeFish();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="TigerShark" /> class.
        /// </summary>
        /// <param name="x">The x coordinate</param>
        /// <param name="y">The y coordinate</param>
        /// <param name="direction">The direction.</param>
        public TigerShark(int x, int y, FishDirection direction)
            : base(x, y, direction)
        {
            this.initializeFish();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="TigerShark" /> class.
        /// </summary>
        public TigerShark()
        {
            this.initializeFish();
        }

        #endregion

        private void initializeFish()
        {
            FishSprite = new TigerSharkSprite(this);
            Type = FishTypes.TigerShark;
        }
    }
}