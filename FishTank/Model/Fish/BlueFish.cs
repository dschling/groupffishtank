﻿using System.Drawing;
using FishTank.Utility.EnumeratedTypes;
using FishTank.Utility.MovementBehaviors;
using FishTank.View.Fish;

namespace FishTank.Model.Fish
{
    /// <summary>
    ///     Holds information about a BlueFish
    /// </summary>
    public class BlueFish : Fish
    {
        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="BlueFish" /> class.
        /// </summary>
        public BlueFish()
        {
            this.initializeFish();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="BlueFish" /> class.
        /// </summary>
        /// <param name="direction">The direction.</param>
        public BlueFish(FishDirection direction) : base(direction)
        {
            this.initializeFish();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="BlueFish" /> class.
        /// </summary>
        /// <param name="location">Location to create fish</param>
        /// <param name="direction">The direction.</param>
        public BlueFish(Point location, FishDirection direction) : base(location, direction)
        {
            this.initializeFish();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="BlueFish" /> class.
        /// </summary>
        /// <param name="x">The x coordinate</param>
        /// <param name="y">The y coordinate</param>
        /// <param name="direction">The direction.</param>
        public BlueFish(int x, int y, FishDirection direction) : base(x, y, direction)
        {
            this.initializeFish();
        }

        #endregion

        private void initializeFish()
        {
            MaxWeight = 20.5;
            MinWeight = 1.5;
            SetRandomWeight();
            Type = FishTypes.BlueFish;
            this.SetDefaultMovement();
            FishSprite = new BlueFishSprite(this);
            FishArea = new Rectangle(X, Y, Width, Height);
            IsSleeping = false;
            IsClicked = false;
            IsBeingBitten = false;
        }

        public override void SetDefaultMovement()
        {
            MovementBehavior = new CasualBehavior();
        }
    }
}