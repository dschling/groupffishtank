﻿using System;
using FishTank.Utility.EnumeratedTypes;

namespace FishTank.Utility
{
    /// <summary>
    ///     A static utility class to generate random integers.
    /// </summary>
    public static class RandomUtils
    {
        #region Data members

        private static readonly Random Randomizer = new Random();

        #endregion

        /// <summary>
        ///     Gets a random int within the specified range.
        /// </summary>
        /// <param name="minValue">The minimum value.</param>
        /// <param name="maxValue">The maximum value.</param>
        /// <returns> A random int </returns>
        public static int NextInt(int minValue, int maxValue)
        {
            return Randomizer.Next(minValue, maxValue);
        }

        /// <summary>
        ///     Gets a random non negative number less than the specified value.
        /// </summary>
        /// <param name="maxValue">The maximum value.</param>
        /// <returns>A random int</returns>
        public static int NextInt(int maxValue)
        {
            return Randomizer.Next(maxValue);
        }

        /// <summary>
        ///     Returns a random double between 0.0 and 1.0
        /// </summary>
        /// <returns></returns>
        public static double NextDouble()
        {
            return Randomizer.NextDouble();
        }

        /// <summary>
        ///     Returns a random double between the maximum and minimum inclusive
        /// </summary>
        /// <returns>a random double between the maximum and minimum inclusive</returns>
        public static double NextDouble(double minimum, double maximum)
        {
            return Randomizer.NextDouble()*(maximum - minimum) + minimum;
        }

        /// <summary>
        ///     Gets the random direction.
        /// </summary>
        /// <returns></returns>
        public static FishDirection GetRandomDirection()
        {
            var directions = (FishDirection[]) Enum.GetValues(typeof (FishDirection));
            var randomDirection = directions[NextInt(directions.Length)];
            return randomDirection;
        }
    }
}