﻿namespace FishTank.Utility.EnumeratedTypes
{
    /// <summary>
    ///     Possible movement directions for Fish
    /// </summary>
    public enum MovementDirection
    {
        /// <summary>
        ///     Up direction
        /// </summary>
        Up,

        /// <summary>
        ///     Down direction
        /// </summary>
        Down,

        /// <summary>
        ///     Forward direction
        /// </summary>
        Forward,

        /// <summary>
        ///     Don't move
        /// </summary>
        None
    }
}