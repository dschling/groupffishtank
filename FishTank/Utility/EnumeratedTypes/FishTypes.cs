﻿namespace FishTank.Utility.EnumeratedTypes
{
    /// <summary>
    ///     Enumerated Fish types.
    /// </summary>
    public enum FishTypes
    {
        /// <summary>
        ///     The shark
        /// </summary>
        Shark,

        /// <summary>
        ///     The tiger shark
        /// </summary>
        TigerShark,

        /// <summary>
        ///     The sword fish
        /// </summary>
        GoldFish,

        /// <summary>
        ///     The spotted gold fish
        /// </summary>
        SpottedGoldFish,

        /// <summary>
        ///     The blue fish
        /// </summary>
        BlueFish,

        /// <summary>
        ///     The chum
        /// </summary>
        Chum
    }
}