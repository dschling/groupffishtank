﻿namespace FishTank.Utility.EnumeratedTypes
{
    /// <summary>
    ///     Directions that a fish can face.
    /// </summary>
    public enum FishDirection
    {
        /// <summary>
        ///     Right facing
        /// </summary>
        Right,

        /// <summary>
        ///     Left facing
        /// </summary>
        Left
    }
}