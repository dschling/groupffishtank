﻿using System;
using System.Linq;
using FishTank.Model.Fish;
using FishTank.Utility.EnumeratedTypes;

namespace FishTank.Utility
{
    /// <summary>
    ///     FishFactory is responsible for creating a
    ///     random type of fish and returning that object.
    /// </summary>
    public static class FishFactory
    {
        /// <summary>
        ///     Gets the random fish.
        /// </summary>
        /// <returns></returns>
        public static Fish GetRandomFish()
        {
            var types = (FishTypes[]) Enum.GetValues(typeof (FishTypes));
            var typesList = types.ToList();
            typesList.Remove(FishTypes.Chum);
            var randomFishType = typesList[RandomUtils.NextInt(typesList.Count)];
            return createFishFromFishType(randomFishType);
        }

        private static Fish createFishFromFishType(FishTypes type)
        {
            switch (type)
            {
                case FishTypes.Shark:
                    return new Shark();

                case FishTypes.TigerShark:
                    return new TigerShark();

                case FishTypes.GoldFish:
                    return new GoldFish();

                case FishTypes.SpottedGoldFish:
                    return new SpottedGoldFish();

                case FishTypes.BlueFish:
                    return new BlueFish();
            }

            throw new ArgumentException("Invalid fish type");
        }
    }
}