﻿using FishTank.Model.Fish;
using FishTank.Utility.EnumeratedTypes;

namespace FishTank.Utility.MovementBehaviors
{
    /// <summary>
    ///     Type of movement for a fish
    /// </summary>
    public abstract class MovementBehavior
    {
        #region Properties

        /// <summary>
        ///     Gets or sets the fish.
        /// </summary>
        /// <value>
        ///     The fish.
        /// </value>
        protected Fish TheFish { get; private set; }

        /// <summary>
        ///     Gets or sets the height of the tank.
        /// </summary>
        /// <value>
        ///     The height of the tank.
        /// </value>
        protected int TankHeight { get; private set; }

        /// <summary>
        ///     Gets or sets the width of the tank.
        /// </summary>
        /// <value>
        ///     The width of the tank.
        /// </value>
        public int TankWidth { get; private set; }

        /// <summary>
        ///     Gets or sets the forward speed.
        /// </summary>
        /// <value>
        ///     The forward speed.
        /// </value>
        protected int ForwardSpeed { get; set; }

        /// <summary>
        ///     Gets or sets the backward speed.
        /// </summary>
        /// <value>
        ///     The backward speed.
        /// </value>
        protected int BackwardSpeed { get; set; }

        /// <summary>
        ///     Gets or sets the downward speed.
        /// </summary>
        /// <value>
        ///     The downward speed.
        /// </value>
        protected int DownwardSpeed { get; set; }

        /// <summary>
        ///     Gets or sets the upward speed.
        /// </summary>
        /// <value>
        ///     The upward speed.
        /// </value>
        protected int UpwardSpeed { get; set; }

        #endregion

        /// <summary>
        ///     Moves the specified fish.
        /// </summary>
        /// <param name="aFish">a fish.</param>
        /// <param name="tankHeight">Height of the tank.</param>
        /// <param name="tankWidth">Width of the tank.</param>
        public virtual void Move(Fish aFish, int tankHeight, int tankWidth)
        {
            this.TheFish = aFish;
            this.TankHeight = tankHeight;
            this.TankWidth = tankWidth;
        }

        /// <summary>
        ///     Moves up.
        /// </summary>
        protected void MoveUp()
        {
            var newY = this.TheFish.Y - this.UpwardSpeed;
            if (newY <= 0)
            {
                return;
            }
            this.TheFish.Y = newY;
        }

        /// <summary>
        ///     Moves down.
        /// </summary>
        protected void MoveDown()
        {
            var newY = this.TheFish.Y + this.DownwardSpeed;
            if (newY + this.TheFish.Height >= this.TankHeight)
            {
                return;
            }
            this.TheFish.Y = newY;
        }

        /// <summary>
        ///     Moves the forward.
        /// </summary>
        protected void MoveForward()
        {
            var newX = this.TheFish.X;
            if (this.TheFish.FishDirection == FishDirection.Right)
            {
                newX += this.ForwardSpeed;
            }
            else
            {
                newX -= this.ForwardSpeed;
            }

            if (newX + this.TheFish.Width >= this.TankWidth || newX <= 0)
            {
                this.TheFish.AddWallHit();
                if (!this.TheFish.IsSleeping)
                {
                    this.TheFish.ChangeDirection();
                }
                return;
            }

            this.TheFish.X = newX;
        }

        /// <summary>
        ///     Moves the backward.
        /// </summary>
        protected void MoveBackward()
        {
            var newX = this.TheFish.X;
            if (this.TheFish.FishDirection == FishDirection.Right)
            {
                newX -= this.BackwardSpeed;
            }
            else
            {
                newX += this.BackwardSpeed;
            }

            if (newX + this.TheFish.X >= this.TankWidth || newX <= 0)
            {
                return;
            }

            this.TheFish.X = newX;
        }
    }
}