﻿using FishTank.Model.Fish;

namespace FishTank.Utility.MovementBehaviors
{
    /// <summary>
    ///     A Resting swim behavior for fish.
    /// </summary>
    public class RestingBehavior : MovementBehavior
    {
        #region Data members

        private const double ForwardPercent = 0.25;
        private const double BackwardPercent = 0.25;
        private const double UpPercent = 0.25;
        private const int RestingForwardSpeed = 2;
        private const int RestingBackwardSpeed = 2;
        private const int RestingUpwardSpeed = 1;
        private const int RestingDownwardSpeed = 1;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="RestingBehavior" /> class.
        /// </summary>
        public RestingBehavior()
        {
            ForwardSpeed = RestingForwardSpeed;
            BackwardSpeed = RestingBackwardSpeed;
            UpwardSpeed = RestingUpwardSpeed;
            DownwardSpeed = RestingDownwardSpeed;
        }

        #endregion

        /// <summary>
        ///     Moves the specified fish.
        /// </summary>
        /// <param name="aFish">a fish.</param>
        /// <param name="tankHeight">Height of the tank.</param>
        /// <param name="tankWidth">Width of the tank.</param>
        public override void Move(Fish aFish, int tankHeight, int tankWidth)
        {
            base.Move(aFish, tankHeight, tankWidth);

            var randomDouble = RandomUtils.NextDouble();

            if (randomDouble <= ForwardPercent)
            {
                MoveForward();
            }
            else if (randomDouble > ForwardPercent && randomDouble <= ForwardPercent + BackwardPercent)
            {
                MoveBackward();
            }
            else if (randomDouble > ForwardPercent && randomDouble <= ForwardPercent + UpPercent + BackwardPercent)
            {
                MoveUp();
            }
            else
            {
                MoveDown();
            }
        }
    }
}