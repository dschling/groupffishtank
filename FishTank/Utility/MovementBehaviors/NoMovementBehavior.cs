﻿namespace FishTank.Utility.MovementBehaviors
{
    /// <summary>
    ///     An empty class that describes a no movement behavior.
    /// </summary>
    public class NoMovementBehavior : MovementBehavior
    {
    }
}