﻿using FishTank.Model.Fish;

namespace FishTank.Utility.MovementBehaviors
{
    /// <summary>
    ///     A casual swim behavior for fish.
    /// </summary>
    public class CasualBehavior : MovementBehavior
    {
        #region Data members

        private const double ForwardPercent = 0.5;
        private const double BackwardPercent = 0.25;
        private const double UpPercent = 0.125;
        private const int CasualForwardSpeed = 5;
        private const int CasualBackwardSpeed = 2;
        private const int CasualUpwardSpeed = 1;
        private const int CasualDownwardSpeed = 1;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="CasualBehavior" /> class.
        /// </summary>
        public CasualBehavior()
        {
            ForwardSpeed = CasualForwardSpeed;
            BackwardSpeed = CasualBackwardSpeed;
            UpwardSpeed = CasualUpwardSpeed;
            DownwardSpeed = CasualDownwardSpeed;
        }

        #endregion

        /// <summary>
        ///     Moves the specified fish.
        /// </summary>
        /// <param name="aFish">a fish.</param>
        /// <param name="tankHeight">Height of the tank.</param>
        /// <param name="tankWidth">Width of the tank.</param>
        public override void Move(Fish aFish, int tankHeight, int tankWidth)
        {
            base.Move(aFish, tankHeight, tankWidth);

            var randomDouble = RandomUtils.NextDouble();

            if (randomDouble <= ForwardPercent)
            {
                MoveForward();
            }
            else if (randomDouble <= ForwardPercent + BackwardPercent)
            {
                MoveBackward();
            }
            else if (randomDouble <= ForwardPercent + UpPercent + BackwardPercent)
            {
                MoveUp();
            }
            else
            {
                MoveDown();
            }
        }
    }
}