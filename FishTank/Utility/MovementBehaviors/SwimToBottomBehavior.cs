﻿using FishTank.Model.Fish;

namespace FishTank.Utility.MovementBehaviors
{
    /// <summary>
    ///     This behavior makes the fish swim to the bottom of the tank.
    /// </summary>
    public class SwimToBottomBehavior : MovementBehavior
    {
        #region Data members

        private const double ForwardPercent = 0.1;
        private const double BackwardPercent = 0.1;
        private const double UpPercent = 0.10;
        private const int BottomForwardSpeed = 2;
        private const int BottomBackwardSpeed = 2;
        private const int BottomUpwardSpeed = 2;
        private const int BottomDownwardSpeed = 10;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="SwimToBottomBehavior" /> class.
        /// </summary>
        public SwimToBottomBehavior()
        {
            ForwardSpeed = BottomForwardSpeed;
            BackwardSpeed = BottomBackwardSpeed;
            UpwardSpeed = BottomUpwardSpeed;
            DownwardSpeed = BottomDownwardSpeed;
        }

        #endregion

        /// <summary>
        ///     Moves the specified fish.
        /// </summary>
        /// <param name="aFish">a fish.</param>
        /// <param name="tankHeight">Height of the tank.</param>
        /// <param name="tankWidth">Width of the tank.</param>
        public override void Move(Fish aFish, int tankHeight, int tankWidth)
        {
            base.Move(aFish, tankHeight, tankWidth);

            var randomDouble = RandomUtils.NextDouble();

            if (randomDouble <= ForwardPercent)
            {
                MoveForward();
            }
            else if (randomDouble <= ForwardPercent + BackwardPercent)
            {
                MoveBackward();
            }
            else if (randomDouble <= ForwardPercent + UpPercent + BackwardPercent)
            {
                MoveUp();
            }
            else
            {
                MoveDown();
            }
        }
    }
}