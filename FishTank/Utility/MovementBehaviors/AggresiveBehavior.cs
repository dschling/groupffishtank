﻿using FishTank.Model.Fish;

namespace FishTank.Utility.MovementBehaviors
{
    /// <summary>
    ///     An aggresive swim behavior for a fish.
    /// </summary>
    public class AggresiveBehavior : MovementBehavior
    {
        #region Data members

        private const double ForwardPercent = 0.95;
        private const double UpPercent = 0.025;
        private const int AggresiveForwardSpeed = 13;
        private const int AggresiveUpwardSpeed = 2;
        private const int AggresiveDownwardSpeed = 2;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="AggresiveBehavior" /> class.
        /// </summary>
        public AggresiveBehavior()
        {
            ForwardSpeed = AggresiveForwardSpeed;
            BackwardSpeed = 0;
            UpwardSpeed = AggresiveUpwardSpeed;
            DownwardSpeed = AggresiveDownwardSpeed;
        }

        #endregion

        /// <summary>
        ///     Moves the specified fish.
        /// </summary>
        /// <param name="aFish">a fish.</param>
        /// <param name="tankHeight">Height of the tank.</param>
        /// <param name="tankWidth">Width of the tank.</param>
        public override void Move(Fish aFish, int tankHeight, int tankWidth)
        {
            base.Move(aFish, tankHeight, tankWidth);
            //base.Move(new Point(aFish.X, aFish.Y));

            var randomDouble = RandomUtils.NextDouble();

            if (randomDouble <= ForwardPercent)
            {
                MoveForward();
            }
            else if (randomDouble <= ForwardPercent + UpPercent)
            {
                MoveUp();
            }
            else
            {
                MoveDown();
            }
        }
    }
}