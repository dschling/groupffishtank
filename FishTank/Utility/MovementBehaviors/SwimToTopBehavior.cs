﻿using FishTank.Model.Fish;

namespace FishTank.Utility.MovementBehaviors
{
    /// <summary>
    ///     This behavior makes the fish swim to the top of the tank.
    /// </summary>
    public class SwimToTopBehavior : MovementBehavior
    {
        #region Data members

        private const double ForwardPercent = 0.10;
        private const double BackwardPercent = 0.10;
        private const double UpPercent = 0.70;
        private const int TopForwardSpeed = 2;
        private const int TopBackwardSpeed = 2;
        private const int TopUpwardSpeed = 10;
        private const int TopDownwardSpeed = 2;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="SwimToTopBehavior" /> class.
        /// </summary>
        public SwimToTopBehavior()
        {
            ForwardSpeed = TopForwardSpeed;
            BackwardSpeed = TopBackwardSpeed;
            UpwardSpeed = TopUpwardSpeed;
            DownwardSpeed = TopDownwardSpeed;
        }

        #endregion

        /// <summary>
        ///     Moves the specified fish.
        /// </summary>
        /// <param name="aFish">a fish.</param>
        /// <param name="tankHeight">Height of the tank.</param>
        /// <param name="tankWidth">Width of the tank.</param>
        public override void Move(Fish aFish, int tankHeight, int tankWidth)
        {
            base.Move(aFish, tankHeight, tankWidth);

            var randomDouble = RandomUtils.NextDouble();

            if (randomDouble <= ForwardPercent)
            {
                MoveForward();
            }
            else if (randomDouble <= ForwardPercent + BackwardPercent)
            {
                MoveBackward();
            }
            else if (randomDouble <= ForwardPercent + UpPercent + BackwardPercent)
            {
                MoveUp();
            }
            else
            {
                MoveDown();
            }
        }
    }
}