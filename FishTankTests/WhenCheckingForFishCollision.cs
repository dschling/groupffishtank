﻿using FishTank.Model;
using FishTank.Model.Fish;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FishTankTests
{
    [TestClass]
    public class WhenCheckingForFishCollision
    {
        #region Data members

        private Fish firstFish;
        private Fish secondFish;

        #endregion

        #region Properties

        private FishTankManager Manager { get; set; }

        #endregion

        [TestInitialize]
        public void Initialize()
        {
            this.Manager = new FishTankManager(400, 400);
            this.Manager.PlaceFishInTank(2);
            this.firstFish = this.Manager.FishData[0];
            this.secondFish = this.Manager.FishData[1];
        }

        [TestMethod]
        public void FishShouldNotBeIntersectingOnInitialization()
        {
            Assert.IsFalse(this.Manager.AreFishColliding());
        }

        [TestMethod]
        public void FishShouldNotBeIntersectingWhenPlacedOffBorderOfEachOther()
        {
            this.firstFish.X = this.secondFish.X - this.firstFish.Width;
            this.firstFish.Y = this.secondFish.Y;
            Assert.IsFalse(this.Manager.AreFishColliding());
        }

        [TestMethod]
        public void FishShouldBeIntersectingWhenPlacedInTheSamePlace()
        {
            this.firstFish.X = this.secondFish.X;
            this.firstFish.Y = this.secondFish.Y;
            Assert.IsTrue(this.Manager.AreFishColliding());
        }

        [TestMethod]
        public void FishShouldBeIntersectingWhenPlacedOnBorderOfEachOther()
        {
            this.firstFish.X = this.secondFish.X - this.firstFish.Width + 1;
            this.firstFish.Y = this.secondFish.Y;
            Assert.IsTrue(this.Manager.AreFishColliding());
        }
    }
}